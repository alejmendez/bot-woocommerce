<?php

namespace App\Controller;

use App\Commands\BotCli;

class BotController
{
    public function __construct() 
    {
        // execute it
        $cli = new BotCli();
        $cli->run();
    }
}
