<?php

namespace App\Process;

use GuzzleHttp\Client;

class BotLicensing
{
    protected $console;
    protected $client;
    protected $clientKeySender;
    protected $keySenderToken;

    public function __construct($console) 
    {
        $this->console = $console;
        $this->initClient();
    }

    protected function initClient()
    {
        $this->client = new Client([
            'base_uri' => $this->getBaseUri(),
            'timeout'  => $this->getTimeout(),
            'auth'     => [$this->getUser(), $this->getPassword()],
        ]);

        $this->clientKeySender = new Client([
            'base_uri' => $this->getBaseUriKeySender(),
            'timeout'  => $this->getTimeoutKeySender(),
        ]);

        $this->keySenderAuth();

        $this->clientKeySender = new Client([
            'base_uri' => $this->getBaseUriKeySender(),
            'timeout'  => $this->getTimeoutKeySender(),
            'headers'  => [
                'Authorization' => $this->keySenderToken
            ]
        ]);
    }

    protected function getBaseUri()
    {
        return env('APIURL');
    }

    protected function getTimeout()
    {
        return env('APITIMEOUT');
    }

    protected function getUser()
    {
        return env('CONSUMERKEY');
    }

    protected function getPassword()
    {
        return env('CONSUMERSECRET');
    }

    protected function getBaseUriKeySender()
    {
        return env('APIURLKEYSENDER');
    }

    protected function getTimeoutKeySender()
    {
        return env('APITIMEOUTKEYSENDER');
    }

    protected function getUserKeySender()
    {
        return env('USERKEYSENDER');
    }

    protected function getPasswordKeySender()
    {
        return env('PASSWORDKEYSENDER');
    }

    public function searchPendingLicenses()
    {
        $this->console->info('Buscando licencias pendientes por entregar');
        $endPoint = 'orders?status=processing';
        $response = $this->client->get($endPoint);
        
        $orders = [];

        if ($response->getStatusCode() == 200) {
            $body = $response->getBody();
            $orders = json_decode($body, true);
        } else {
            $this->console->error('Error al intentar traer los pedidos pendientes');
        }

        return $orders;
    }

    public function keySenderAuth()
    {
        $this->console->info('auth en keysender');
        $endPoint = 'login?email=' . $this->getUserKeySender() . '&password=' . $this->getPasswordKeySender();
        $response = $this->clientKeySender->post($endPoint);
        
        if ($response->getStatusCode() == 200) {
            $body = $response->getBody();
            $res = json_decode($body, true);
            $this->keySenderToken = $res['token_type'] . ' ' . $res['access_token'];
        } else {
            $this->console->error('Error al autenticarse en keysender');
        }
    }

    public function sendKeys($order)
    {
        $this->console->info('Enviando Clave por keysender');
        
        $lineItems = $order['line_items'];
        foreach ($lineItems as $lineItem) {
            $product = $this->getProduct($lineItem['product_id']);
            $result  = $this->sendKeyLineItem($order, $lineItem, $product);
        }

        return true;
    }

    public function sendKeyLineItem($order, $lineItem, $product)
    {
        $endPoint = 'transaction/addcustom';
        $database_id = '';
        $template_id = '6143';
        foreach ($product['attributes'] as $attribute) {
            if ($attribute['name'] == 'database_id') {
                $database_id = $attribute['options'][0];
            }

            if ($attribute['name'] == 'template_id') {
                $template_id = $attribute['options'][0];
            }
        }

        if ($database_id == '') {
            $this->console->error('El articulo no posee database_id');
        }

        $params = [
            'payer'       => $order['billing']['email'],
            'quantity'    => $lineItem['quantity'],
            'database_id' => $database_id,
            'amount'      => $lineItem['total'],
            'currency'    => $order['currency'],
            'name'        => $order['billing']['first_name'] . ' ' . $order['billing']['last_name'],
            'template_id' => $template_id,
            // 'msgsubject'  => '¡Gracias por tu compa!',
            // 'msgtext'     => 'Haz adquirido el producto ' . $product['name']
        ];
        $response = $this->clientKeySender->post($endPoint, [
            'json' => $params
        ]);
        
        if ($response->getStatusCode() == 200) {
            $this->console->info('Clave enviada satisfactoriamente');
            $body = $response->getBody();
            $res = json_decode($body, true);
            return true;
        } else {
            $this->console->error('Error al enviar la key');
        }

        return false;
    }

    public function getProduct($productId)
    {
        $this->console->info('Buscando el producto ' . $productId);
        $endPoint = 'products/' . $productId;
        $response = $this->client->get($endPoint);
        
        $product = [];

        if ($response->getStatusCode() == 200) {
            $body = $response->getBody();
            $product = json_decode($body, true);
        } else {
            $this->console->error('Error al intentar traer el producto ' . $productId);
        }

        return $product;
    }

    public function completedOrder($order)
    {
        $this->console->info('Cambiando de estado la orden ' . $order['id']);

        $endPoint = 'orders/' . $order['id'];
        $response = $this->client->put($endPoint, [
            'form_params' => [
                'status' => 'completed'
            ]
        ]);

        if ($response->getStatusCode() == 200) {
            $body = $response->getBody();
            $order = json_decode($body, true);
            return true;
        } else {
            $this->console->error('Error al intentar cambiar el estado de la orden ' . $order['id']);
        }

        return false;
    }
}
