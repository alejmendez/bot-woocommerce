<?php

namespace App\Commands;

use splitbrain\phpcli\CLI;
use splitbrain\phpcli\Options;

use App\Process\BotLicensing;

class BotCli extends CLI
{
    protected $version = '1.0.0';
    protected $bot;

    // register options and arguments
    protected function setup(Options $options)
    {
        $options->setHelp('Bot para woocommerce');
        $options->registerOption(
            'version', 
            'print version', 
            'v'
        );

        $options->registerOption(
            'licensingProcess', 
            'proceso de envío de licencias', 
            'l'
        );
    }

    // implement your code
    protected function main(Options $options)
    {
        if ($options->getOpt('version')) {
            $this->version();
        } elseif ($options->getOpt('licensingProcess')) {
            $this->licensingProcess();
        } else {
            echo $options->help();
        }
    }

    protected function version()
    {
        $this->info('1.0.0');
    }

    protected function licensingProcess()
    {
        $this->info('Iniciando el proceso de licencia');
        $this->bot = new BotLicensing($this);

        $orders = $this->bot->searchPendingLicenses();
        foreach ($orders as $order) {
            $sendKey = $this->bot->sendKeys($order);
            
            if ($sendKey) {
                $this->bot->completedOrder($order);
            }
        }
    }
}
