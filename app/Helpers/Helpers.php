<?php

function env($key, $default = '') 
{
    return isset($_ENV[$key]) ? $_ENV[$key] : $default;
}